# Contribution Guidelines

Here you can find the guidelines to be followed when developing new features/bugfixes for this project.

[[_TOC_]]

## Node version

This project is written with Node.js v14.
Other versions of Node.js might not give the same results.

## NPM scripts

This project uses `yarn` throughout the code base.
To install it globally using `npm`:

```bash
npm i -g yarn
```

### Project setup

[Git LFS](https://git-lfs.github.com/) is used for large binary files such as images and videos.
Make sure it is installed, then use the following command to initialise it:

```bash
git lfs install
```

You only have to do this once per repository per user.

Just run the `yarn` command to install all dependencies.

```bash
yarn
```

Now everything is in place to start programming.

### Compiles and hot-reloads for development

```bash
yarn serve
```

### Compiles and minifies for production

```bash
yarn build
```

### Fixes style for all files

```bash
yarn style:fix
```

## Git Guidelines

### The Flow

The flow used in this repository is based off of [GitFlow](https://nvie.com/posts/a-successful-git-branching-model/), created by Vincent Driessen.

### Branches

- **prod:** The production branch containing production ready code.
- **dev (default):** The development branch containing features and fixes that are ready for the next release.
- **feature/...:** Branches containing changes linked to a certain new feature.
- **bugfix/...:** Branches containing changes linked to a certain bug report.
- **chore/...:** Branches containing changes linked to a certain task that is neither a feature nor a bugfix.
- **release/...:** Branches containing changes to prepare for a new production release.
- **hotfix/...:** Branches containing changes linked to a certain bug report, these handle severe bugs on the production branch.

`prod` and `dev` are not pushable by **anyone**.

### Commit Rules

Commit messages are linted and checked upon commiting your changes.
The linting is based off of [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).

**Form:** type\[(scope)\]\[!\]: ...

- **type:** This defines the type of changes made in the commit. Valid options are:
  - `feat`: You added a new feature.
  - `fix`: You fixed something.
  - `test`: You added/updated tests.
  - `refactor`: You did some refactoring.
  - `perf`: You made changes to improve performance.
  - `style`: You made style changes.
  - `docs`: You added/updated documentation.
  - `build`: You made changes that affect the build system.
  - `ci`: You made changes to the CI configuration.
  - `chore`: You did some other chore.
- **scope (OPTIONAL):** This defines the commit's scope.
- **! (OPTIONAL):** This indicates that the commit contains breaking changes.

#### Examples

feat: this is my commit

fix!: this is my breaking commit

chore(scope): this is my scoped commit

perf(scope)!: this is my scoped breaking commit

### Steps for implementing a new feature

1. Make sure your local `dev` branch is up to date.
2. Create a new branch from `dev`, name it `feature/#n-...` where `n` is the issue ID and end it with a short description of the feature.
3. Make your changes.
4. Make sure your local `dev` branch is up to date and rebase `dev` into your branch.
5. When you're ready create a merge request for your branch to merge into `dev`.
6. Add the new feature to the [changelog](CHANGELOG.md) and reference the created merge request.
7. Discuss and fix any issues the reviewers might see in your code.
8. Wait for the feature to be merged.

Don't create your `feature` branches from any other branch than `dev` to prevent git :spaghetti:!

### Steps for implementing a new bugfix

1. Make sure your local `dev` branch is up to date.
2. Create a new branch from `dev`, name it `bugfix/#n-...` where `n` is the issue ID and end it with a short description of the bug to be fixed.
3. Make your changes.
4. Make sure your local `dev` branch is up to date and rebase `dev` into your branch.
5. When you're ready create a merge request for your branch to merge into `dev`.
6. Add the new bugfix to the [changelog](CHANGELOG.md) and reference the created merge request.
7. Discuss and fix any issues the reviewers might see in your code.
8. Wait for the bugfix to be merged.

Don't create your `bugfix` branches from any other branch than `dev` to prevent git :spaghetti:!

### Steps for implementing a new chore

1. Make sure your local `dev` branch is up to date.
2. Create a new branch from `dev`, name it `chore/#n-...` where `n` is the issue ID and end it with a short description of the chore.
3. Make your changes.
4. Make sure your local `dev` branch is up to date and rebase `dev` into your branch.
5. When you're ready create a merge request for your branch to merge into `dev`.
6. Add the new chore to the [changelog](CHANGELOG.md) and reference the created merge request.
7. Discuss and fix any issues the reviewers might see in your code.
8. Wait for the chore to be merged.

Don't create your `chore` branches from any other branch than `dev` to prevent git :spaghetti:!

### Steps for implementing a new release (ONLY MAINTAINERS)

1. Make sure your local `dev` branch is up to date.
2. Create a new branch from `dev`, name it `release/vM.m.p` where `M.m.p` follows [SemVer](https://semver.org/) formatting.
3. Bump the version.
4. Fix potential last minute issues.
5. When you're ready create a merge request for your branch to merge into `prod`.
6. Update the [changelog](CHANGELOG.md) and reference the created merge request next to the version number.
7. Discuss and fix any issues the reviewers might see in your code.
8. Wait for the new release to be merged.

Don't create your `release` branches from any other branch than `dev` to prevent git :spaghetti:!

### Steps for implementing a new hotfix (ONLY MAINTAINERS)

1. Make sure your local `prod` branch is up to date.
2. Create a new branch from `prod`, name it `hotfix/#n-...` where `n` is the issue ID and end it with a short description of the hotfix.
3. Bump the version.
4. Make your changes.
5. Make sure your local `prod` branch is up to date and rebase `prod` into your branch.
6. When you're ready create a merge request for your branch to merge into `dev`.
7. Add the new hotfix to the [changelog](CHANGELOG.md) and reference the created merge request.
8. Discuss and fix any issues the reviewers might see in your code.
9. Wait for the hotfix to be merged.

Don't create your `hotfix` branches from any other branch than `prod` to prevent git :spaghetti:!

## Project Guidelines

### General

- Add an **MIT License** reference to any newly added files in the form of a comment on the very first line of the file.
- Add an **Author** comment right underneath the license on newly added files and right above newly created classes, functions, methods, components, mixins, ...

### Webpack Aliases

A few extra aliases are added to simplify import statements.
Here is a list of all the available aliases.

- `@`: [`src/`](src/)
- `@c`: [`src/components/consumer/`](src/components/consumer/)
- `@p`: [`src/components/provider/`](src/components/provider/)
- `@v`: [`src/views/`](src/views/)
- `@m`: [`src/mixins/`](src/mixins/)
- `@u`: [`src/util/`](src/util/)
- `@x`: [`src/store/`](src/store/)
- `@xm`: [`src/store/modules/`](src/store/modules/)
- `@r`: [`src/router/`](src/router/)
- `#`: [`tests/`](tests/)
- `#u`: [`tests/unit/`](tests/unit/)

### Components

Components are created with the [Provider/Consumer pattern](https://learn-vuejs.github.io/vue-patterns/patterns/#provider-consumer) in mind.

This means that functionality and presentation are seperated.

The presentation (_Consumer_) is a functional component referencing the `parent` component's state.

```vue
<template functional>...</template>
```

The functionality (_Provider_) is a renderless component that only renders a default slot and exposes a `state` and `actions` as computed properties.

```vue
<script>
export default {
  mixins: [...],
  props: {...},

  computed: {
    state() {
      return {
        ...this.props,
        ...
      }
    },

    actions() {
      return {...}
    }
  },

  methods: {...},

  render() {
    return this.$scopedSlots.default({
      state: this.state,
      actions: this.actions
    });
  }
}
</script>
```

### View Components

When a component is linked to a certain [route](https://router.vuejs.org/) then it is a view component and should be located in the `src/views` directory.

View components are where the _Provider_ and _Consumer_ components are combined.

```vue
<template>
  <Provider v-slot="{ state, actions }">
    <Consumer v-bind="{ state, actions }" />
  </Provider>
</template>

<script>
import Provider from '@p/...';
import Consumer from '@c/...';

export default {
  ...
  components: {
    Provider,
    Consumer
  },
  ...
}
</script>
```

Routes are to be lazy-loaded, this is done in the `router/index.js` file.

```js
...
export const NEW_PATH_NAME = '...'
const routes = {
  ...
  {
    path: '...',
    name: NEW_PATH_NAME,
    component: () => import('@v/...')
  }
  ...
};
```

### Vuex Store

Everything in the [Vuex store](https://vuex.vuejs.org/) is separated into modules. If you want to add something to store, add it to the correct module,
or create a new one when appropriate.

Mutations should never be accessed by code outside the Vuex module. Only use `state`, `getters`, and `actions`
when manipulating or querying application state anywhere in the application.

**store/modules/...**

```js
export const NAME_MODULE = '...';

export const ACTION_NAME = '...';
export const GETTER_NAME = '...';

const MUTATION_NAME = '...';

export default {
  namespaced: true,

  state: {...},
  getters: {
    [GETTER_NAME]: (state) => {...}
  },
  mutations: {
    [MUTATION_NAME]: (state, payload) => {...}
  },
  actions: {
    [ACTION_NAME]: ({ commit }, payload) => {
      commit(MUTATION_NAME, payload);
    }
  }
}
```

### Internationalization

The application uses [Vue-i18n](https://kazupon.github.io/vue-i18n/introduction.html) to support multiple languages.

The language configuration files are located in [`src/i18n/`](src/i18n/), one file per language.

The linter checks for literal strings within the HTML components and throws an error when such strings are found.
You should always use the global `$t()` function for text that will be visible on the web page.

```vue
<template>
  <p>{{ this.$t('...') }}</p>
</template>
```

### Material Framework

The application uses [Vuetify](https://vuetifyjs.com/en/getting-started/installation/) as its Material Design Framework.

The Vuetify components should only be used in the _Consumer_ components.

You are free to use the global Vuetify options within the _View_ Components.

## Testing

This repository strives for a 100% total code coverage,
so try to cover as much of your code as you can in your unit tests.

### Unit Testing

The unit tests are written in [Mocha](https://mochajs.org/) + [Chai](https://www.chaijs.com/), using Chai's [Expect syntax](https://www.chaijs.com/api/bdd/).
Additionally, [Sinon.js](https://sinonjs.org/) is used for mocking, stubbing, and spying functions.

For the testing of Vue components [Vue Test Utils](https://vue-test-utils.vuejs.org/) is used.

The tests can be run with the following commands:

```bash
yarn test:unit
yarn test:unit:cov  # generates a coverage report in text format
```

These commands also allow you to pass files or directories as arguments for running specific tests.
This is especially useful during development.

If you want to generate additional coverage reports then run these commands:

```bash
yarn cov:cobertura  # generates a coverage report in cobertura format
yarn cov:html       # generates a coverage report in html format
```

#### Writing Unit Tests

You should write unit tests for all your code that resides in the `src` directory.
You should name the test file the same as the original, but with the `.spec.js` extension.
Your test file should be placed in the [`tests/unit`](tests/unit) directory, following the same directory structure you used in the `src` directory.

**Example:** `src/path/to/my-code.js` --> `tests/unit/path/to/my-code.spec.js`

The [`tests/unit/index.js`](tests/unit/index.js) file contains some common functions you can use within your tests to simplify them.

- **mount**: Wraps the `vue-test-util` mount function and provides some default options to prevent boilerplate.
  - _usage without extra options_: `mount(Component)`
  - _usage with extra options_: `mount(Component, { localVue, ... })`
- **mountProvider**: Wraps the `vue-test-util` mount function and provides some default options to prevent boilerplate. Specifically for Provider components.
  - _usage without extra options_: `mountProvider(Component)`
  - _usage with extra options_: `mountProvider(Component, { localVue, ... })`
- **fakeStore**: Creates a Vuex store.
  - _usage_: `fakeStore(localVue, { state: {...}, getters: {...}, mutations: {...}, actions: {...}, modules: {...} })`
- **exec**: Executes a component function without mounting it.
  - _usage_: `exec(Component.methods.myMethod, localThis, ...args)`
  - _usage without args_: `exec(Component.methods.myMethod, localThis)`
  - _usage without args or this_: `exec(Component.methods.myMethod)`
- **checkEvents**: Executes a component function without mounting it, and tracks the emitted events. Returns an object with arrays of arrays with the passed arguments per event.
  - _return example_: `{ 'click': [ [0], [1], [2] ], 'myEvent': [['a', 1], ['b', 2]] }`, 5 events were emitted in total: 3 `click` events, 2 `myEvent` events. `click` was first emitted with argument `(0)`, then with `(1)`, then with `(2)`. `myEvent` was first emitted with arguments `('a', 1)`, then with `('b', 2)`.
  - _usage_: `checkEvents(Component.methods.myMethod, localThis, ...args)`
  - _usage without args_: `checkEvents(Component.methods.myMethod, localThis)`
  - _usage without args or this_: `checkEvents(Component.methods.myMethod)`
- **validate**: Tests a component prop's validation. Only works on props with custom validators. Returns a boolean representing whether the value was valid or not.
  - _usage_: `validate(Component.props.myProp, valueToValidate)`
- **createCommit**: Creates a `commit` function for testing Vuex actions.
  - _usage_: `createCommit(vuexModule, initialState)`

When testing components try to write as little tests as possible that mount the component.
Preferably write **one** test to check if the component can successfully mount, and use the common functions to unit test the component's internal functions.

### End-to-End Testing

The end-to-end tests are written in [WebDriverIO](https://webdriver.io/) using the Mocha framework (BDD).
These tests use WebDriverIO's [Expect syntax](https://webdriver.io/docs/api/expect-webdriverio.html).

The e2e tests can be run in Google Chrome and Mozilla Firefox:

```bash
yarn test:e2e:chrome
yarn test:e2e:firefox
```

The tests can also be run headless by passing the `--headless` option.

Tests are run for 5 different screen sizes.
By default the tests are run for all screen sizes, by passing the `--sizes xs,sm,md,lg,xl` you can specify which screen sizes to run the tests for.
Or you could use the corresponding `yarn` commands to run the tests for a single screen size.

_Screen Sizes:_

- `xs`
- `sm`
- `md`
- `lg`
- `xl`

_Examples:_

- `yarn test:e2e:firefox`: run E2E tests with Firefox for all screen sizes.
- `yarn test:e2e:chrome --sizes xs,xl`: run E2E tests with Google Chrome for the `xs` and `xl` screen sizes.
- `yarn test:e2e:chrome:md`: run E2E tests with Google Chrome for the `md` screen size.
- `yarn test:e2e:firefox --headless`: run E2E tests with Firefox in headless mode for all screen sizes.

More info on running tests can be found [here](https://webdriver.io/docs/organizingsuites.html).
The CLI options for the `wdio` command can also be passed to the `yarn test:e2e:*` commands.

#### Writing End-to-End Tests

E2E tests are written per endpoint/page, usually 1 test file per view component.
Give your test files a logical name, like the name of the corresponding view.
The name should be in `kebab-case`, with the `.spec.js` extension, e.g. `my-endpoint.spec.js`.
Your tests should be placed in the [`tests/e2e/specs`](tests/e2e/specs) directory.

The E2E tests use the [Page Object Pattern](https://webdriver.io/docs/pageobjects.html).
Every page should have a corresponding Page class, which inherits from the main `Page` class (located in [`tests/e2e/pageobjects/.page.js`](tests/e2e/pageobjects/.page.js)).
Your page object classes should reside in their own file, named in `kebab-case` with the `.page.js` extension,
and should be placed in the [`tests/e2e/pageobjects`](tests/e2e/pageobjects) directory.

**Example:**

- `src/view/MyView.vue`: view component which is attached to a route
- `tests/e2e/pageobjects/my-view.page.js`: Page Object file for the endpoint
- `tests/e2e/specs/my-view.spec.js`: E2E tests for the endpoint

The [`tests/e2e/index.js`](tests/e2e/index.js) contains some functions to simplify your E2E tests.

- **describeE2E**: Wraps `mocha`'s `describe` function, extending it to automatically run tests for different screen sizes. For more info about writing tests with the `describeE2E` function see [`tests/e2e/example.spec.js`](tests/e2e/example.spec.js).
- **download**: Downloads a file from a given URL to a given destination file.
- **deleteFile**: Deletes the given file.

Try to always use the provided `describeE2E` function to simplify your tests and to ensure that your tests are run on different screen sizes.
