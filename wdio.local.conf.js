const { config } = require('./wdio.shared.conf');
const { config: firefox } = require('./wdio/wdio.firefox.conf');
const { config: chrome } = require('./wdio/wdio.chrome.conf');

let browsers;

if (process.argv.includes('--firefox')) browsers = firefox;
else if (process.argv.includes('--chrome')) browsers = chrome;

exports.config = {
  /**
   * base config
   */
  ...config,
  /**
   * config for local testing
   */
  ...browsers
};
