# Changelog

## v0.1.0

### Features

- **World list:** See a list of created worlds (!4)
- **Add world:** Add an item to the world list (!5, !20)
- **Delete world:** Remove an item from the world list (!7, !9)
- **World details:** See world details (!10)
- **Apply new style:** Apply new styling (!14)
- **Character list:** See a list of created characters (!15)
- **Edit world details:** Edit world details (!17)
- **Character Details:** See character details (!19)
