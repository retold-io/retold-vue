# Release

[//]: # 'Add the release version to the Title above'

## Checklist

[//]: # 'This is a list of things to be checked by the reviewers'
[//]: # 'Please leave everything unchecked'

- [ ] Documentation has been added/updated where necessary.
- [ ] All texts and comments are in English.
- [ ] The [Guidelines](CONTRIBUTING.md) have been followed.
- [ ] Added files reference license and initial author.
- [ ] HTML components don't use string literals but i18n variables.
- [ ] Merging into `prod`.
- [ ] MR is tagged correctly.
- [ ] CHANGELOG has been updated.
- [ ] Version has been bumped.
