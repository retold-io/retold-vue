// MIT License
// Author: Nick Van Osta

import { deepClone } from '@u/deep-object';

import { expect } from 'chai';
import { deepMerge } from '../../../src/util/deep-object';

describe('Utility - Deep Object', () => {
  describe('deepClone', () => {
    it('should correctly clone an undeep object', async () => {
      const a = { a: 1, b: 2, c: 3 };
      const b = deepClone(a);

      expect(a).to.not.equal(b);
      expect(a).to.deep.equal(b);
    });

    it('should correctly clone an undeep array', async () => {
      const a = [1, 2, 3];
      const b = deepClone(a);

      expect(a).to.not.equal(b);
      expect(a).to.deep.equal(b);
    });

    it('should correctly clone a non-object value', async () => {
      const a = 123;
      const b = deepClone(a);

      expect(a).to.equal(b);
    });

    it('should correctly deep clone an array with nested arrays and objects', async () => {
      const a = [1, [2, 3], { d: 4, e: 5 }, () => {}];
      const b = deepClone(a);

      expect(a).to.not.equal(b);
      expect(a).to.deep.equal(b);
    });

    it('should correctly deep clone an object with nested arrays and objects', async () => {
      const a = { a: 1, b: [2, 3], c: { d: 4, e: 5 }, f: () => {} };
      const b = deepClone(a);

      expect(a).to.not.equal(b);
      expect(a).to.deep.equal(b);
    });
  });

  describe('deepMerge', () => {
    it('should correctly merge 2 undeep objects', async () => {
      const a = { a: 1, b: 2, c: 3 };
      const b = { c: 4, d: 5, e: 6 };
      const expected = { a: 1, b: 2, c: 4, d: 5, e: 6 };

      const c = deepMerge(a, b);
      expect(c).to.deep.equal(expected);
    });

    it('should correctly merge 2 undeep arays', async () => {
      const a = [1, 2, 3];
      const b = [4, 5, 6];
      const expected = [1, 2, 3, 4, 5, 6];

      const c = deepMerge(a, b);
      expect(c).to.deep.equal(expected);
    });

    it('should correctly merge 2 non-object values', async () => {
      const a = 1;
      const b = 2;
      const expected = 2;

      const c = deepMerge(a, b);
      expect(c).to.deep.equal(expected);
    });

    it('should correctly deep clone an array with nested arrays and objects', async () => {
      const f1 = () => {};
      const f2 = () => {};
      const a = [1, [2, 3], { d: 4, e: 5 }, f1];
      const b = [1, [2, 3], { d: 4, e: 5 }, f2];
      const expected = [
        1,
        [2, 3],
        { d: 4, e: 5 },
        f1,
        1,
        [2, 3],
        { d: 4, e: 5 },
        f2
      ];

      const c = deepMerge(a, b);
      expect(c).to.deep.equal(expected);
    });

    it('should correctly deep clone an object with nested arrays and objects', async () => {
      const f1 = () => {};
      const f2 = () => {};
      const f3 = () => {};
      const f4 = () => {};
      const a = {
        a: 1,
        b: [2, 3],
        c: { d: 4, e: 5 },
        f: f1,
        g: 1,
        h: [2, 3],
        i: { j: 4, k: 5 },
        j: f2
      };
      const b = {
        a: 26,
        b: [25, 24],
        c: { e: 23, f: 22 },
        f: f3,
        w: 1,
        x: [2, 3],
        y: { j: 4, k: 5 },
        z: f4
      };
      const expected = {
        a: 26,
        b: [2, 3, 25, 24],
        c: { d: 4, e: 23, f: 22 },
        f: f3,
        g: 1,
        h: [2, 3],
        i: { j: 4, k: 5 },
        j: f2,
        w: 1,
        x: [2, 3],
        y: { j: 4, k: 5 },
        z: f4
      };

      const c = deepMerge(a, b);
      expect(c).to.deep.equal(expected);
    });

    it('should correctly deep clone an object with non-matching types for properties', async () => {
      const a = {
        a: 1,
        b: 2,
        c: [3, 4],
        d: { e: 5, f: 6 }
      };
      const b = {
        a: { e: 5, f: 6 },
        b: [1, 2],
        c: 3,
        d: 4
      };

      const c = deepMerge(a, b);
      expect(c).to.deep.equal(b);
    });

    it('should correctly merge objects with circular references', async () => {
      const a = { a: { b: { c: {} } } };
      a.a.b.c = a;
      const b = { a: { b: { c: {} } } };
      b.a.b.c = b;

      const c = deepMerge(a, b);
      expect(c).to.deep.equal(b);
    });
  });
});
