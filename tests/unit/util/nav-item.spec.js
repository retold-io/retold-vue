// MIT License
// Author: Nick Van Osta

import NavItem from '@u/nav-item';

import { expect } from 'chai';

describe('Utility - NavItem', () => {
  describe('constructor', () => {
    it('should create a new NavItem correctly', async () => {
      const to = 'route';
      const title = 'title';
      const icon = 'icon';

      const nav = new NavItem(to, title, icon);
      expect(nav.to).to.equal(to);
      expect(nav.title).to.equal(title);
      expect(nav.icon).to.equal(icon);
    });

    it('should create a new NavItem correctly without optional params', async () => {
      const to = 'route';
      const title = 'title';

      const nav = new NavItem(to, title);
      expect(nav.to).to.equal(to);
      expect(nav.title).to.equal(title);
      expect(nav.icon).to.be.a('string');
    });
  });
});
