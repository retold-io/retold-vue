// MIT License
// Author: Nick Van Osta

import Gradient from '@u/gradient';

import { expect } from 'chai';

describe('Utility - Gradient', () => {
  describe('constructor', () => {
    it('should create a new Gradient correctly', async () => {
      const color = { from: 'from', to: 'to' };
      const opacity = { from: 0.123, to: 0.456 };

      const gradient = new Gradient(color, opacity);
      expect(gradient.color.from).to.equal(color.from);
      expect(gradient.color.to).to.equal(color.to);
      expect(gradient.opacity.from).to.equal(opacity.from);
      expect(gradient.opacity.to).to.equal(opacity.to);
    });

    it('should create a new Gradient correctly without optional params', async () => {
      const color = { from: 'from', to: 'to' };

      var gradient = new Gradient(color);
      expect(gradient.color.from).to.equal(color.from);
      expect(gradient.color.to).to.equal(color.to);
      expect(gradient.opacity.from).to.be.a('number');
      expect(gradient.opacity.to).to.be.a('number');

      const opacityFrom = { from: 0.123 };
      gradient = new Gradient(color, opacityFrom);
      expect(gradient.color.from).to.equal(color.from);
      expect(gradient.color.to).to.equal(color.to);
      expect(gradient.opacity.from).to.equal(opacityFrom.from);
      expect(gradient.opacity.to).to.be.a('number');

      const opacityTo = { to: 0.123 };
      gradient = new Gradient(color, opacityTo);
      expect(gradient.color.from).to.equal(color.from);
      expect(gradient.color.to).to.equal(color.to);
      expect(gradient.opacity.from).to.be.a('number');
      expect(gradient.opacity.to).to.equal(opacityTo.to);
    });
  });
});
