// MIT License
// Author: Nick Van Osta

import FabItem from '@u/fab-item';

import { expect } from 'chai';

describe('Utility - FabItem', () => {
  describe('constructor', () => {
    it('should create a new FabItem correctly', async () => {
      const title = 'title';
      const icon = 'icon';
      const action = () => {};
      const color = 'color';

      const card = new FabItem(title, icon, action, color);
      expect(card.title).to.equal(title);
      expect(card.icon).to.equal(icon);
      expect(card.action).to.equal(action);
      expect(card.color).to.equal(color);
    });

    it('should create a new FabItem correctly without optional params', async () => {
      const title = 'title';
      const icon = 'icon';
      const action = () => {};

      const card = new FabItem(title, icon, action);
      expect(card.title).to.equal(title);
      expect(card.icon).to.equal(icon);
      expect(card.action).to.equal(action);
      expect(card.color).to.be.a('string');
    });
  });
});
