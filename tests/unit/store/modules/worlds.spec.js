// MIT License
// Author: Nick Van Osta

import worlds, * as module from '@xm/worlds';
import World from '@m/world';

import { createCommit } from '#u';
import { expect } from 'chai';
import sinon from 'sinon';

const sandbox = sinon.createSandbox();

const createState = () => ({ list: {} });
const testWorld = new World(0, 'Testopia', '', 'Unit test world');

describe('Vuex - Worlds', () => {
  afterEach(async () => {
    sandbox.restore();
  });

  it('GET WORLD Getter', async () => {
    const state = createState();
    state.list[0] = 'my world';

    const world = worlds.getters[module.GET_WORLD](state)(0);
    expect(world).to.equal(state.list[0]);
  });

  it('ADD WORLD Action', async () => {
    const state = createState();
    const commit = createCommit(worlds, state);
    expect(Object.keys(state.list)).to.be.empty;

    // from empty list
    worlds.actions[module.ADD_WORLD]({ commit, state }, testWorld);
    expect(Object.keys(state.list)).to.not.be.empty;
    expect(state.list[1].id).to.equal(1);

    worlds.actions[module.ADD_WORLD]({ commit, state }, testWorld);
    expect(Object.keys(state.list)).to.not.be.empty;
    expect(state.list[2].id).to.equal(2);
  });

  it('SET WORLD Action', async () => {
    const state = createState();
    const commit = createCommit(worlds, state);
    const id = testWorld.id;
    state.list[id] = testWorld;
    expect(Object.keys(state.list)).to.not.be.empty;

    const newName = 'New Testopia';
    const newThumbnail = 'New Thumbnail';
    const newDescription = 'Just like Old Testopia, but newer';
    const updatedWorld = new World(id, newName, newThumbnail, newDescription);

    worlds.actions[module.SET_WORLD]({ commit, state }, updatedWorld);
    expect(Object.keys(state.list)).to.not.be.empty;
    expect(state.list[id].id).to.equal(id);
    expect(state.list[id].name).to.equal(newName);
    expect(state.list[id].thumbnail).to.equal(newThumbnail);
    expect(state.list[id].description).to.equal(newDescription);
  });

  it('DEL WORLD Action', async () => {
    const state = createState();
    const commit = createCommit(worlds, state);
    state.list[0] = testWorld;
    expect(Object.keys(state.list)).to.not.be.empty;

    worlds.actions[module.DEL_WORLD]({ commit, state }, 0);
    expect(Object.keys(state.list)).to.be.empty;
  });
});
