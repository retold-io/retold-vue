// MIT License
// Author: Nick Van Osta

import appbar, { SET_TITLE, SET_IMG, RESET_TITLE, RESET_IMG } from '@xm/appbar';

import { createCommit } from '#u';
import { expect } from 'chai';
import sinon from 'sinon';

const sandbox = sinon.createSandbox();

const createState = () => ({ title: 'TITLE', img: 'IMG' });

describe('Vuex - AppBar', () => {
  afterEach(async () => {
    sandbox.restore();
  });

  it('SET TITLE Action', async () => {
    const state = createState();
    const commit = createCommit(appbar, state);
    const newTitle = 'FANCY NEW TITLE';
    appbar.actions[SET_TITLE]({ commit, state }, newTitle);

    expect(state.title).to.equal(newTitle);
  });

  it('SET IMG Action', async () => {
    const state = createState();
    const commit = createCommit(appbar, state);
    const newImg = 'FANCY NEW IMG';
    appbar.actions[SET_IMG]({ commit, state }, newImg);

    expect(state.img).to.equal(newImg);
  });

  it('RESET TITLE Action', async () => {
    const state = createState();
    const commit = createCommit(appbar, state);
    appbar.actions[RESET_TITLE]({ commit, state });

    expect(state.title).to.equal('Retold');
  });

  it('RESET IMG Action', async () => {
    const state = createState();
    const commit = createCommit(appbar, state);
    appbar.actions[RESET_IMG]({ commit, state });

    expect(state.img).to.equal('');
  });
});
