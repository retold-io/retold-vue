// MIT License
// Author: Nick Van Osta

import store from '@x';

import { expect } from 'chai';

describe('Vuex', () => {
  it('should return the Vuex store', async () => {
    expect(store).to.exist;
  });
});
