// MIT License
// Author: Nick Van Osta

import WorldEdit from '@v/WorldEdit';

// import World from '@m/world';

import { WORLDS_MODULE, GET_WORLD, SET_WORLD } from '@xm/worlds';

import { createLocalVue } from '@vue/test-utils';
import { mount, fakeStore, exec } from '#u';
import { expect } from 'chai';
import sinon from 'sinon';

const sandbox = sinon.createSandbox();

describe('View - WorldEdit', () => {
  const localVue = createLocalVue();

  afterEach(async () => {
    sandbox.restore();
  });

  it('should successfully mount the component', async () => {
    const mockWorldsModule = {
      namespace: true,

      getters: {
        [GET_WORLD]: sandbox.fake()
      },

      actions: {
        [SET_WORLD]: sandbox.fake()
      }
    };

    const store = fakeStore(localVue, {
      modules: {
        [WORLDS_MODULE]: mockWorldsModule
      }
    });
    const wrapper = mount(WorldEdit, {
      localVue,
      store,

      propsData: {
        id: 0
      },

      stubs: {
        CWorldDialog: true
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;
  });

  it('should return the selected world to edit', async () => {
    const localThis = {
      id: 0,
      [GET_WORLD]: sandbox.fake()
    };
    exec(WorldEdit.computed.world, localThis);
    expect(localThis[GET_WORLD].calledOnceWithExactly(localThis.id)).to.be.true;
  });

  it('should correctly open/close dialog', async () => {
    const clock = sandbox.useFakeTimers();
    const localThis = { $router: { push: sinon.fake() } };

    exec(WorldEdit.watch.dialog, localThis, true);
    clock.runAll();
    expect(localThis.$router.push.calledOnce).to.be.false;
    localThis.$router.push.resetHistory();

    exec(WorldEdit.watch.dialog, localThis, false);
    clock.runAll();
    expect(localThis.$router.push.calledOnce).to.be.true;
  });

  it('should close the dialog when canceled', async () => {
    const localThis = { dialog: 123 };
    exec(WorldEdit.methods.cancel, localThis);
    expect(localThis.dialog).to.be.false;
  });

  it('should close the dialog and save the world when saved', async () => {
    const localThis = { dialog: 123, [SET_WORLD]: sinon.fake() };
    const world = 'TEST WORLD';
    exec(WorldEdit.methods.save, localThis, world);

    expect(localThis.dialog).to.be.false;
    expect(localThis[SET_WORLD].calledOnce).to.be.true;
  });
});
