// MIT License
// Author: Nick Van Osta

import WorldList from '@v/WorldList';

import CCardListAddItem from '@c/CardListAddItem';
import CCardListAddFab from '@c/CardListAddFab';

import World from '@m/world';
import CardItem from '@u/card-item';

import { APPBAR_MODULE, SET_TITLE } from '@xm/appbar';
import { DEL_WORLD } from '@xm/worlds';

import { WORLDS_EDIT } from '@r';

import { createLocalVue } from '@vue/test-utils';
import { mount, fakeStore, exec } from '#u';
import { expect } from 'chai';
import sinon from 'sinon';

const sandbox = sinon.createSandbox();

describe('View - WorldList', () => {
  const localVue = createLocalVue();

  afterEach(async () => {
    sandbox.restore();
  });

  it('should successfully mount the component', async () => {
    const mockAppbarModule = {
      namespaced: true,

      actions: {
        [SET_TITLE]: sandbox.fake()
      }
    };

    const store = fakeStore(localVue, {
      modules: {
        [APPBAR_MODULE]: mockAppbarModule
      }
    });

    const wrapper = mount(WorldList, {
      localVue,
      store,

      stubs: {
        CCardList: true,
        CCardListItem: true,
        CCardListDelete: true
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;
  });

  it('should show the correct add consumer on desktop', async () => {
    const localThis = {
      $vuetify: {
        breakpoint: {
          mobile: false
        }
      }
    };

    const component = exec(WorldList.computed.addConsumer, localThis);
    expect(component).to.equal(CCardListAddItem);

    const id = exec(WorldList.computed.addId, localThis);
    expect(id).to.equal('world-add-item');
  });

  it('should show the correct add consumer on mobile', async () => {
    const localThis = {
      $vuetify: {
        breakpoint: {
          mobile: true
        }
      }
    };

    const component = exec(WorldList.computed.addConsumer, localThis);
    expect(component).to.equal(CCardListAddFab);

    const id = exec(WorldList.computed.addId, localThis);
    expect(id).to.equal('world-add-fab');
  });

  it('should successfully execute the beforeCreate lifecycle hook', async () => {
    const localThis = {
      $store: {
        hasModule: () => false,
        registerModule: sandbox.fake()
      }
    };

    exec(WorldList.beforeCreate[0], localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.true;

    localThis.$store.hasModule = () => true;
    localThis.$store.registerModule.resetHistory();
    exec(WorldList.beforeCreate[0], localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.false;
  });

  it('should successfully execute the created lifecycle hook', async () => {
    const localThis = {
      [SET_TITLE]: sandbox.fake(),
      $t: () => {}
    };
    exec(WorldList.created, localThis);

    expect(localThis[SET_TITLE].calledOnce).to.be.true;
  });

  it('should successfully execute the mounted lifecycle hook', async () => {
    const localThis = {
      $store: {
        hasModule: () => false,
        registerModule: sandbox.fake()
      }
    };

    exec(WorldList.mounted, localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.true;

    localThis.$store.hasModule = () => true;
    localThis.$store.registerModule.resetHistory();
    exec(WorldList.mounted, localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.false;
  });

  it('should successfully execute the beforeDestroy lifecycle hook', async () => {
    const localThis = {
      $store: {
        unregisterModule: sandbox.fake()
      }
    };
    exec(WorldList.beforeDestroy, localThis);

    expect(localThis.$store.unregisterModule.calledOnce).to.be.true;
  });

  it('should correctly map the vuex world list', async () => {
    let worlds;
    const localThis = {
      $store: {
        state: {}
      },
      worldItems: {}
    };
    worlds = exec(WorldList.computed.worldList, localThis);
    expect(worlds).to.be.an('array');
    expect(worlds).to.be.empty;

    localThis.worldItems = { 1: {} };
    worlds = exec(WorldList.computed.worldList, localThis);
    expect(worlds).to.be.an('array');
    expect(worlds.length).to.equal(1);
  });

  it('should convert all worlds into card items', async () => {
    const localThis = {
      worlds: [
        new World(1, '1', '1', '1', {}),
        new World(2, '2', '2', '2', {}),
        new World(3, '3', '3', '3', {})
      ]
    };

    let world;
    const cards = exec(WorldList.computed.worldItems, localThis);
    Object.values(cards).forEach((c, i) => {
      world = localThis.worlds[i];
      expect(c instanceof CardItem).to.be.true;
      expect(c.id).to.equal(world.id);
      expect(c.title).to.equal(world.name);
      expect(c.img).to.equal(world.thumbnail);
    });
  });

  it('should create the correct gradient for the card images', async () => {
    const localThis = {
      $vuetify: {
        theme: {
          currentTheme: {
            primary: 'primary',
            accent: 'accent'
          }
        }
      }
    };

    const gradient = exec(WorldList.computed.cardGradient, localThis);
    expect(gradient.color.from).to.equal(
      localThis.$vuetify.theme.currentTheme.primary
    );
    expect(gradient.color.to).to.equal(
      localThis.$vuetify.theme.currentTheme.accent
    );
    expect(gradient.opacity.from).to.be.a('number');
    expect(gradient.opacity.to).to.be.a('number');
  });

  it('should go to the edit route', async () => {
    const localThis = { $router: { push: sandbox.fake() } };
    const id = 420;

    exec(WorldList.methods.editWorld, localThis, id);
    expect(
      localThis.$router.push.calledOnceWithExactly({
        name: WORLDS_EDIT,
        params: { id }
      })
    ).to.be.true;
  });

  it('should prepare a world for deletion', async () => {
    const localThis = {
      deleteDialog: null,
      toDelete: null
    };
    const id = 123;

    exec(WorldList.methods.deleteWorld, localThis, id);
    expect(localThis.deleteDialog).to.be.true;
    expect(localThis.toDelete).to.equal(id);
  });

  it('should confirm the deletion of a world', async () => {
    window.URL.revokeObjectURL = sandbox.stub().returns('revoked');

    const localThis = {
      [DEL_WORLD]: sandbox.fake()
    };
    const world = new World(1, 'title', 'thumbnail', 'description', {});

    exec(WorldList.methods.confirmDelete, localThis, world);
    expect(localThis[DEL_WORLD].calledOnce).to.be.true;
    expect(window.URL.revokeObjectURL.calledOnce).to.be.true;
  });
});
