// MIT License
// Author Xander Veldeman

import CharacterList from '@v/CharacterList';
import Character from '@m/character';

import CardItem from '@u/card-item';

import { deepMerge, deepClone } from '@u/deep-object';
import appbar, { SET_TITLE } from '@xm/appbar';

import { createLocalVue } from '@vue/test-utils';
import { mount, fakeStore, exec } from '#u';
import { expect } from 'chai';
import sinon from 'sinon';

const sandbox = sinon.createSandbox();

describe('View - Character List', () => {
  const localVue = createLocalVue();
  let store;
  let mockStore;

  beforeEach(async () => {
    mockStore = {
      actions: {
        [SET_TITLE]: sandbox.fake()
      }
    };

    store = fakeStore(localVue, {
      modules: {
        appbar: deepMerge(deepClone(appbar), mockStore)
      }
    });
  });

  afterEach(async () => {
    sandbox.restore();
  });

  it('should succesfully mount the component', async () => {
    const wrapper = mount(CharacterList, {
      localVue,
      store,

      stubs: {
        CCardList: true,
        CCardListItem: true
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;
  });

  it('should successfully execute the beforeCreate lifecycle hook', async () => {
    const localThis = {
      $store: {
        hasModule: () => false,
        registerModule: sandbox.fake()
      }
    };

    exec(CharacterList.beforeCreate[0], localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.true;

    localThis.$store.hasModule = () => true;
    localThis.$store.registerModule.resetHistory();
    exec(CharacterList.beforeCreate[0], localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.false;
  });

  it('should successfully execute the created lifecycle hook', async () => {
    const localThis = {
      [SET_TITLE]: mockStore.actions[SET_TITLE],
      $t: () => {}
    };
    exec(CharacterList.created, localThis);

    expect(localThis[SET_TITLE].calledOnce).to.be.true;
  });

  it('should successfully execute the mounted lifecycle hook', async () => {
    const localThis = {
      $store: {
        hasModule: () => false,
        registerModule: sandbox.fake()
      }
    };

    exec(CharacterList.mounted, localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.true;

    localThis.$store.hasModule = () => true;
    localThis.$store.registerModule.resetHistory();
    exec(CharacterList.mounted, localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.false;
  });

  it('should correctly map the vuex character list', async () => {
    let characters;
    const localThis = {
      $store: {
        state: {}
      }
    };
    characters = exec(CharacterList.computed.characters, localThis);
    expect(characters).to.be.null;

    localThis.$store.state.characters = { list: [] };
    characters = exec(CharacterList.computed.characters, localThis);
    expect(characters).to.be.an('array');
  });

  it('should convert all characters into card items', async () => {
    const localThis = {
      characters: [
        new Character(1, '1', '1', '1', '1', '1', {}),
        new Character(2, '2', '2', '2', '2', '2', {}),
        new Character(3, '3', '3', '3', '3', '3', {})
      ]
    };

    let character;
    const cards = exec(CharacterList.computed.characterItems, localThis);
    Object.values(cards).forEach((c, i) => {
      character = localThis.characters[i];
      expect(c instanceof CardItem).to.be.true;
      expect(c.id).to.equal(character.id);
      expect(c.title).to.equal(character.name);
      expect(c.img).to.equal(character.thumbnail);
    });
  });

  it('should create the correct gradient for the card images', async () => {
    const localThis = {
      $vuetify: {
        theme: {
          currentTheme: {
            primary: 'primary',
            accent: 'accent'
          }
        }
      }
    };

    const gradient = exec(CharacterList.computed.cardGradient, localThis);
    expect(gradient.color.from).to.equal(
      localThis.$vuetify.theme.currentTheme.primary
    );
    expect(gradient.color.to).to.equal(
      localThis.$vuetify.theme.currentTheme.accent
    );
    expect(gradient.opacity.from).to.be.a('number');
    expect(gradient.opacity.to).to.be.a('number');
  });
});
