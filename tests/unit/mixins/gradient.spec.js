// MIT License
// Author: Nick Van Osta

import gradient from '@mx/gradient';
import Gradient from '@u/gradient';

import { exec } from '#u';
import { expect } from 'chai';

describe('Mixin - Gradient', () => {
  it('should successfully create a gradient string with opacity', async () => {
    const gradientString = exec(
      gradient.methods.createGradient,
      {},
      new Gradient({ from: '#000000', to: '#ffffff' }, { from: 0.5, to: 0.75 })
    );

    expect(gradientString.from).to.equal('rgba(0, 0, 0, 0.5)');
    expect(gradientString.to).to.equal('rgba(255, 255, 255, 0.75)');
  });

  it('should successfully create a gradient string without opacity', async () => {
    const gradientString = exec(
      gradient.methods.createGradient,
      {},
      new Gradient({ from: '#000000', to: '#ffffff' })
    );

    expect(gradientString.from).to.equal('#000000');
    expect(gradientString.to).to.equal('#ffffff');
  });

  it("should throw an exception when the parameters aren't the correct type", async () => {
    const fn = () =>
      exec(
        gradient.methods.createGradient,
        {},
        { from: '#000000', to: '#ffffff' }
      );

    expect(fn).to.throw();
  });
});
