// MIT License
// Author: Nick Van Osta

import App from '@/App';

import PNavBottom from '@p/AppNavBottom';
import PFooter from '@p/AppFooter';
import CNavBottom from '@c/AppNavBottom';
import CFooter from '@c/AppFooter';

import appbar from '@xm/appbar';
import nav from '@xm/nav';
import footer from '@xm/footer';

import { createLocalVue } from '@vue/test-utils';
import { mount, fakeStore, exec } from '#u';
import { expect } from 'chai';

describe('Component - App', () => {
  const localVue = createLocalVue();
  let store;

  beforeEach(async () => {
    store = fakeStore(localVue, {
      modules: {
        appbar: { state: appbar.state },
        nav: { state: nav.state },
        footer: { state: footer.state }
      }
    });
  });

  it('should successfully mount the component', async () => {
    const wrapper = mount(App, {
      localVue,
      store,

      stubs: {
        CAppBar: true,
        CNavDrawer: true,
        CNavBottom: true,
        CFooter: true
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;
  });

  it('should show the correct bottom component on desktop', async () => {
    let footer;
    const localThis = {
      $vuetify: {
        theme: {
          currentTheme: {
            secondary: 'secondary',
            accent: 'accent'
          }
        },
        breakpoint: {
          mobile: false
        }
      }
    };

    footer = exec(App.computed.bottomProvider, localThis);
    expect(footer).to.equal(PFooter);

    footer = exec(App.computed.bottomConsumer, localThis);
    expect(footer).to.equal(CFooter);

    const color = exec(App.computed.bottomColor, localThis);
    expect(color).to.be.a('string');

    const id = exec(App.computed.bottomId, localThis);
    expect(id).to.equal('footer');
  });

  it('should show the correct bottom component on mobile', async () => {
    let navBottom;
    const localThis = {
      $vuetify: {
        theme: {
          currentTheme: {
            secondary: 'secondary',
            accent: 'accent'
          }
        },
        breakpoint: {
          mobile: true
        }
      }
    };

    navBottom = exec(App.computed.bottomProvider, localThis);
    expect(navBottom).to.equal(PNavBottom);

    navBottom = exec(App.computed.bottomConsumer, localThis);
    expect(navBottom).to.equal(CNavBottom);

    const color = exec(App.computed.bottomColor, localThis);
    expect(color).to.be.a('string');

    const id = exec(App.computed.bottomId, localThis);
    expect(id).to.equal('nav-bottom');
  });
});
