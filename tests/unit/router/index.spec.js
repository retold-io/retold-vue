// MIT License
// Author: Nick Van Osta

import router from '@r';

import { expect } from 'chai';
import sinon from 'sinon';

const sandbox = sinon.createSandbox();

const route = {
  params: {
    id: '123',
    lang: 'en'
  }
};

const emptyRoute = { params: {} };

const checkComponents = (r) => {
  expect(r.component()).to.exist;
  r.children && r.children.forEach(checkComponents);
};
const checkProps = (r) => {
  r.props && expect(r.props(route)).to.exist;
  r.children && r.children.forEach(checkProps);
};

describe('Vue Router', () => {
  afterEach(async () => {
    sandbox.restore();
  });

  it('should correctly import the components', async () => {
    router.options.routes.forEach(checkComponents);
  });

  it('should correctly parse the route props', async () => {
    router.options.routes.forEach(checkProps);
  });

  it('should correctly execute the beforeEach hooks', async () => {
    const next = sandbox.stub();
    await router.beforeHooks.forEach(async (h) => {
      await h(route, route, next);
      expect(next.calledOnce).to.be.true;
      next.reset();

      await h(emptyRoute, emptyRoute, next);
      expect(next.calledOnce).to.be.true;
      next.reset();
    });
  });
});
