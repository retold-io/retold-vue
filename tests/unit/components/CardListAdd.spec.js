// MIT License
// Author: Nick Van Osta

import CardListAdd from '@p/CardListAdd';

import { createLocalVue } from '@vue/test-utils';
import { mountProvider } from '#u';
import { expect } from 'chai';

describe('Component - CardListAdd', () => {
  const localVue = createLocalVue();

  it('should successfully mount the component', async () => {
    const wrapper = mountProvider(CardListAdd, {
      localVue
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;
  });
});
