// MIT License
// Author: Nick Van Osta

import CardListDelete from '@p/CardListDelete';

import CardItem from '@u/card-item';

import { createLocalVue } from '@vue/test-utils';
import { mountProvider, exec, checkEvents } from '#u';
import { expect } from 'chai';

describe('Component - CardListDelete', () => {
  const localVue = createLocalVue();

  it('should successfully mount the component', async () => {
    const data = new CardItem(0, 'TITLE', 'IMG');
    const color = 'Test color';
    const errorColor = 'Error color';

    const wrapper = mountProvider(CardListDelete, {
      localVue,

      propsData: {
        data,
        color,
        errorColor
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;

    expect(wrapper.vm.state.data).to.equal(data);
    expect(wrapper.vm.state.color).to.equal(color);
    expect(wrapper.vm.state.errorColor).to.equal(errorColor);
  });

  it('should correctly validate before deletion', async () => {
    const localThis = { text: 'WRONG TITLE', data: { title: 'TITLE' } };

    let valid = exec(CardListDelete.computed.valid, localThis);
    expect(valid).to.be.false;

    localThis.text = localThis.data.title;
    valid = exec(CardListDelete.computed.valid, localThis);
    expect(valid).to.be.true;
  });

  it('should emit the input event with its value', async () => {
    const localThis = { value: false };
    const events = checkEvents(CardListDelete.methods.input, localThis);

    expect(events['input']).to.not.be.empty;
    expect(events['input'][0]).to.not.be.empty;
    expect(events['input'][0][0]).to.be.true;
  });

  it('should correctly cancel the deletion', async () => {
    const localThis = { text: 'lorem ipsum' };
    const events = checkEvents(CardListDelete.methods.cancel, localThis);

    expect(localThis.text).to.be.empty;
    expect(events['input']).to.not.be.empty;
    expect(events['input'][0]).to.not.be.empty;
    expect(events['input'][0][0]).to.be.false;
  });

  it('should correctly confirm the deletion when valid', async () => {
    const localThis = {
      data: { string: 'string', number: 1 },
      text: 'lorem ipsum',
      valid: true
    };
    const events = checkEvents(CardListDelete.methods.confirm, localThis);

    expect(localThis.text).to.be.empty;
    expect(events['input']).to.not.be.empty;
    expect(events['input'][0]).to.not.be.empty;
    expect(events['input'][0][0]).to.be.false;
    expect(events['delete']).to.not.be.empty;
    expect(events['delete'][0]).to.not.be.empty;
    expect(events['delete'][0][0]).to.equal(localThis.data);
  });

  it('should not confirm the deletion when invalid', async () => {
    const localThis = {
      text: 'lorem ipsum',
      valid: false
    };
    const events = checkEvents(CardListDelete.methods.confirm, localThis);

    expect(localThis.text).to.not.be.empty;
    expect(events['input']).to.not.exist;
    expect(events['delete']).to.not.exist;
  });

  it('should set the text correctly', async () => {
    const txt = 'THIS IS TEXT';
    const localThis = { text: '' };

    exec(CardListDelete.methods.setText, localThis, txt);
    expect(localThis.text).to.equal(txt);
  });
});
