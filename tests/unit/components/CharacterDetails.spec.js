// MIT License
// Author: Nick Van Osta, Xander Veldeman

import CharacterDetails from '@p/CharacterDetails';

import Character from '@m/character';

import { createLocalVue } from '@vue/test-utils';
import { mountProvider } from '#u';
import { expect } from 'chai';

describe('Component - CharacterDetails', () => {
  const localVue = createLocalVue();

  it('should successfully mount the component', async () => {
    const character = new Character(
      1,
      'TEST',
      'RACETEST',
      'GENDERTEST',
      'THUMBNAIL',
      'DESCRIPTION',
      'THEME'
    );

    const wrapper = mountProvider(CharacterDetails, {
      localVue,

      propsData: {
        character
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;

    expect(wrapper.vm.state.character).to.equal(character);
  });
});
