// MIT License
// Author: Nick Van Osta

import AppNavBottom from '@p/AppNavBottom';

import NavItem from '@u/nav-item';

import { createLocalVue } from '@vue/test-utils';
import { mountProvider, validate } from '#u';
import { expect } from 'chai';

describe('Component - NavBottom', () => {
  const localVue = createLocalVue();

  it('should successfully mount the component', async () => {
    const color = 'COLOR';

    const wrapper = mountProvider(AppNavBottom, {
      localVue,

      propsData: {
        color
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;

    expect(wrapper.vm.state.color).to.equal(color);
  });

  it('should correcly validate the links', async () => {
    const valid = [
      new NavItem('LINK', 'TITLE', 'ICON'),
      new NavItem('LINK', 'TITLE', 'ICON')
    ];
    const invalid = [new NavItem('LINK', 'TITLE', 'ICON'), 'Hi ._.'];

    expect(validate(AppNavBottom.props.links, valid)).to.be.true;
    expect(validate(AppNavBottom.props.links, invalid)).to.be.false;
  });
});
