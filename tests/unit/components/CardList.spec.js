// MIT License
// Author: Nick Van Osta

import CardList from '@p/CardList';

import CardItem from '@u/card-item';

import { createLocalVue } from '@vue/test-utils';
import { mountProvider, validate } from '#u';
import { expect } from 'chai';

describe('Component - CardList', () => {
  const localVue = createLocalVue();

  it('should successfully mount the component', async () => {
    const wrapper = mountProvider(CardList, {
      localVue
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;
  });

  it('should correctly validate the data prop', async () => {
    const valid = [
      new CardItem(0, 'TEST', 'IMG'),
      new CardItem(1, 'TEST', 'IMG')
    ];
    const invalid = [new CardItem(0, 'TEST', 'IMG'), 'Hi ._.'];

    expect(validate(CardList.props.data, valid)).to.be.true;
    expect(validate(CardList.props.data, invalid)).to.be.false;
  });
});
