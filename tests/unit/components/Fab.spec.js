// MIT License
// Author: Nick Van Osta

import Fab from '@p/Fab';

import FabItem from '@u/fab-item';

import { createLocalVue } from '@vue/test-utils';
import { mountProvider, exec, validate } from '#u';
import { expect } from 'chai';
import { createSandbox } from 'sinon';

const sandbox = createSandbox();

describe('Component - Fab', () => {
  const localVue = createLocalVue();

  afterEach(async () => {
    sandbox.restore();
  });

  it('should successfully mount the component', async () => {
    const wrapper = mountProvider(Fab, {
      localVue
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;
  });

  it('should correctly validate the items', async () => {
    const valid = [
      new FabItem('TEST', 'ICON', sandbox.fake()),
      new FabItem('TEST', 'ICON', sandbox.fake())
    ];
    const invalid = [new FabItem('TEST', 'ICON', sandbox.fake()), 'Hi ._.'];

    expect(validate(Fab.props.items, valid)).to.be.true;
    expect(validate(Fab.props.items, invalid)).to.be.false;
  });

  it('should open/close when toggled', async () => {
    const localThis = { open: false };

    // open
    exec(Fab.methods.toggle, localThis);
    expect(localThis.open).to.be.true;

    // close
    exec(Fab.methods.toggle, localThis);
    expect(localThis.open).to.be.false;
  });
});
