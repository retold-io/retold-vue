// MIT License
// Author: Xander Veldeman

const CharacterList = require('../pageobjects/character-list.page');

const { describeE2E } = require('..');

describeE2E('Character List Page', () => {
  beforeEach(async () => {
    await CharacterList.open();
  });

  it('should open and render a list of characters', async () => {
    expect(CharacterList.body).toBeDisplayed();
    expect(CharacterList.list).toBeDisplayed();

    (await CharacterList.characters).forEach((w) => expect(w).toBeDisplayed);
  });
});
