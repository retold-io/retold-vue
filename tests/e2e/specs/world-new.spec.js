// MIT License
// Author: Nick Van Osta

const WorldNew = require('../pageobjects/world-new.page');

const { describeE2E, download, deleteFile } = require('..');
const path = require('path');

const worldName = 'TEST WORLD';
const worldImgURL = 'https://media.giphy.com/media/Vuw9m5wXviFIQ/giphy.gif';
const worldImg = path.resolve('./world-img.gif');
const worldDescription = 'Testy McTestFace';

let dialog;
let tabInfo;
let tabTheme;
let tabPreview;
let inputWorldName;
let inputWorldImg;
let inputWorldDescription;
let inputThemeButtons;
let inputThemeSwitch;
let inputColorPicker;
let worldPreview;
let close;
let cancel;
let confirm;

const refreshElements = async () => {
  dialog = await WorldNew.dialog;
  tabInfo = await WorldNew.tabInfo;
  tabTheme = await WorldNew.tabTheme;
  tabPreview = await WorldNew.tabPreview;
  inputWorldName = await WorldNew.inputName;
  inputWorldImg = await WorldNew.inputImg;
  inputWorldDescription = await WorldNew.inputDescription;
  inputThemeSwitch = await WorldNew.inputThemeSwitch;
  inputColorPicker = await WorldNew.inputColorPicker;
  inputThemeButtons = ['primary', 'secondary', 'accent', 'background'].map(
    async (c) => await WorldNew.getThemeButton(c)
  );

  worldPreview = await WorldNew.worldPreview;

  close = await WorldNew.close;
  cancel = await WorldNew.cancel;
  confirm = await WorldNew.confirm;
};

const addDialogOpen = async () => {
  await refreshElements();
  expect(dialog).toBeDisplayed();
  expect(tabInfo).toBeDisplayed();
  expect(tabTheme).toBeDisplayed();
  expect(tabPreview).toBeDisplayed();
  expect(inputWorldName).toBeDisplayed();
  expect(inputWorldImg).toBeDisplayed();
  expect(inputWorldDescription).toBeDisplayed();
  expect(inputThemeSwitch).toBeDisplayed();
  expect(inputColorPicker).toBeDisplayed();
  inputThemeButtons.forEach((btn) => expect(btn).toBeDisplayed());

  expect(worldPreview).toBeDisplayed();

  expect(close).toBeDisplayed();
  expect(cancel).toBeDisplayed();
  expect(confirm).toBeDisplayed();
};

const addDialogClosed = async () => {
  await refreshElements();
  expect(dialog).not.toBeDisplayed();
  expect(tabInfo).not.toBeDisplayed();
  expect(tabTheme).not.toBeDisplayed();
  expect(tabPreview).not.toBeDisplayed();
  expect(inputWorldName).not.toBeDisplayed();
  expect(inputWorldImg).not.toBeDisplayed();
  expect(inputWorldDescription).not.toBeDisplayed();
  expect(inputThemeSwitch).not.toBeDisplayed();
  expect(inputColorPicker).not.toBeDisplayed();
  inputThemeButtons.forEach((btn) => expect(btn).not.toBeDisplayed());

  expect(worldPreview).not.toBeDisplayed();

  expect(close).not.toBeDisplayed();
  expect(cancel).not.toBeDisplayed();
  expect(confirm).not.toBeDisplayed();
};

const setThemeColor = async (btn, r, g, b) => {
  await btn.click();
  await inputColorPicker[0].setValue(r);
  await inputColorPicker[1].setValue(g);
  await inputColorPicker[2].setValue(b);

  expect(btn.getCSSProperty('background-color')).toBe(
    `#${r.toString(16)}${g.toString(16)}${b.toString(16)}`
  );
};

describeE2E(
  'New World Page',
  () => {
    before(async () => {
      await download(worldImgURL, worldImg);
    });

    after(async () => {
      await deleteFile(worldImg);
    });

    beforeEach(async () => {
      await WorldNew.open();
    });

    it('should add the world correctly', async () => {
      const worldsBefore = await WorldNew.worlds;

      // dialog should start closed
      await addDialogOpen();

      expect(confirm).toBeClickable();

      // input data
      await tabInfo.click();
      await inputWorldName.addValue(worldName);
      await inputWorldImg.addValue(worldImg);
      await inputWorldDescription.addValue(worldDescription);

      await tabTheme.click();
      inputThemeButtons.forEach(
        async (btn) => await setThemeColor(btn, 0, 0, 0)
      );
      await inputThemeSwitch.click();
      inputThemeButtons.forEach(
        async (btn) => await setThemeColor(btn, 255, 255, 255)
      );

      await tabPreview.click();

      // confirm
      await confirm.click();

      // dialog should close
      await addDialogClosed();

      // check if world was added
      const worldsAfter = await WorldNew.worlds;
      expect(worldsAfter.length).toBeGreaterThan(worldsBefore.length);
    });

    it('should close the dialog correctly', async () => {
      const worldsBefore = await WorldNew.worlds;

      // dialog should start closed
      await addDialogOpen();

      expect(cancel).toBeClickable();

      // input data
      await inputWorldName.addValue(worldName);
      await inputWorldImg.addValue(worldImg);

      // confirm
      await close.click();

      // dialog should close
      await addDialogClosed();

      // check that world was not added
      const worldsAfter = await WorldNew.worlds;
      expect(worldsAfter.length).toStrictEqual(worldsBefore.length);
    });
  },
  {
    desktop: () => {
      it('should cancel the dialog correctly', async () => {
        const worldsBefore = await WorldNew.worlds;

        // dialog should start closed
        await addDialogOpen();

        expect(cancel).toBeClickable();

        // input data
        await inputWorldName.addValue(worldName);
        await inputWorldImg.addValue(worldImg);

        // confirm
        await cancel.click();

        // dialog should close
        await addDialogClosed();

        // check that world was not added
        const worldsAfter = await WorldNew.worlds;
        expect(worldsAfter.length).toStrictEqual(worldsBefore.length);
      });
    }
  }
);
