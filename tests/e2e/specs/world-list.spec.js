// MIT License
// Author: Nick Van Osta

const WorldList = require('../pageobjects/world-list.page');

const { describeE2E } = require('..');

let dialog;
let inputTitle;
let close;
let cancel;
let confirm;

let title;

const refreshElements = async () => {
  dialog = await WorldList.delete;
  inputTitle = await WorldList.deleteInput;
  close = await WorldList.deleteClose;
  cancel = await WorldList.deleteCancel;
  confirm = await WorldList.deleteConfirm;
};

const deleteDialogOpen = async () => {
  await refreshElements();
  expect(dialog).toBeDisplayed();
  expect(inputTitle).toBeDisplayed();
  expect(cancel).toBeDisplayed();
  expect(close).toBeDisplayed();
  expect(confirm).toBeDisplayed();
};

const deleteDialogClosed = async () => {
  await refreshElements();
  expect(dialog).not.toBeDisplayed();
  expect(inputTitle).not.toBeDisplayed();
  expect(cancel).not.toBeDisplayed();
  expect(close).not.toBeDisplayed();
  expect(confirm).not.toBeDisplayed();
};

const clickHover = async (cardItem, btn) => {
  await cardItem.scrollIntoView({ block: 'center', inline: 'center' });
  await cardItem.moveTo();
  await btn.click();
};

const getRandomWorld = async () => {
  const worlds = await WorldList.worlds;
  // don't pick index 0, it's unclickable for wdio for some reason...
  return worlds[Math.floor(Math.random() * (worlds.length - 1)) + 1];
};

describeE2E(
  'World List Page',
  () => {
    beforeEach(async () => {
      await WorldList.open();
    });

    it('should open and render', async () => {
      expect(WorldList.body).toBeDisplayed();
      expect(WorldList.list).toBeDisplayed();
      expect(WorldList.delete).not.toBeDisplayed();

      (await WorldList.worlds).forEach((w) => expect(w).toBeDisplayed());
    });

    it('should open the new world dialog', async () => {
      const newWorldMock = browser.mock('/worlds/new');
      const add = await WorldList.add;
      add.click();

      expect(newWorldMock).toBeRequested(1);
    });

    it('should open the edit world dialog', async () => {
      const editWorldMock = browser.mock('/worlds/**/edit');

      const toEdit = await getRandomWorld();
      const editBtn = await toEdit.$('#edit');

      await clickHover(toEdit, editBtn);

      expect(editWorldMock).toBeRequested(1);
    });

    it('should delete the correct world', async () => {
      // dialog should start closed
      await deleteDialogClosed();

      // decide random world to delete
      const worldsBefore = await WorldList.worlds;
      const toDelete = await getRandomWorld();
      const deleteBtn = await toDelete.$('#delete');

      // delete the world
      await clickHover(toDelete, deleteBtn);
      await deleteDialogOpen();

      title = await inputTitle.getAttribute('placeholder');
      await inputTitle.addValue(title);
      await confirm.click();

      await deleteDialogClosed();

      // check whether the correct world element was removed
      const worldsAfter = await WorldList.worlds;

      expect(worldsAfter.length).toBeLessThan(worldsBefore.length);
      expect(worldsAfter).not.toContain(toDelete);
    });

    it('should close the dialog correctly', async () => {
      // dialog should start closed
      await deleteDialogClosed();

      // decide random world to delete
      const worldsBefore = await WorldList.worlds;
      const toDelete = await getRandomWorld();
      const deleteBtn = await toDelete.$('#delete');

      // close dialog midway through
      await clickHover(toDelete, deleteBtn);
      expect(cancel).toBeClickable();
      expect(close).toBeClickable();
      expect(confirm).toBeClickable();

      title = await inputTitle.getAttribute('placeholder');
      await inputTitle.addValue(title);
      await close.click();

      await deleteDialogClosed();

      // check that no world was removed
      const worldsAfter = await WorldList.worlds;

      expect(worldsAfter.length).toStrictEqual(worldsBefore.length);
      expect(worldsAfter).not.toContain(toDelete);
    });
  },
  {
    mobile: () => {
      it('should display a FAB for adding worlds', async () => {
        expect(WorldList.addFab).toBeDisplayed();
        expect(WorldList.addItem).not.toBeDisplayed();
      });

      it('should at all times display the delete button', async () => {
        const deleteButtons = (await WorldList.worlds).map((w) =>
          w.$('#delete')
        );

        deleteButtons.forEach((d) => expect(d).toBeDisplayed());
      });
    },

    desktop: () => {
      it('should display a card for adding worlds', async () => {
        expect(WorldList.addFab).not.toBeDisplayed();
        expect(WorldList.addItem).toBeDisplayed();
      });

      it('should only display the delete button when the card is hovered over', async () => {
        let del;
        (await WorldList.worlds).forEach((w) => {
          del = w.$('#delete');

          expect(del).not.toBeDisabled();
          w.moveTo();
          expect(del).toBeDisabled();
        });
      });

      it('should cancel the deletion correctly', async () => {
        // dialog should start closed
        await deleteDialogClosed();

        // decide random world to delete
        const worldsBefore = await WorldList.worlds;
        const toDelete = await getRandomWorld();
        const deleteBtn = await toDelete.$('#delete');

        // cancel deletion midway through
        await clickHover(toDelete, deleteBtn);
        await deleteDialogOpen();

        title = await inputTitle.getAttribute('placeholder');
        await inputTitle.addValue(title);
        await cancel.click();

        await deleteDialogClosed();

        // check that no world was removed
        const worldsAfter = await WorldList.worlds;

        expect(worldsAfter.length).toStrictEqual(worldsBefore.length);
        expect(worldsAfter).not.toContain(toDelete);
      });
    }
  }
);
