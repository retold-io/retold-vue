// MIT License
// Author: Nick Van Osta

const { describeE2E } = require('.');

describeE2E(
  'Example tests',
  (suite) => {
    // suite contains some parameters you can use:
    //  suite.size: string containing the screen size ['xs', 'sm', 'md', 'lg', 'xl']
    //  suite.mobile: boolean that indicates whether the browser is mobile or not
    console.log(suite.size);
    console.log(suite.mobile);

    before(() => console.log('before everything'));
    beforeEach(() => console.log('before each screen size suite'));
    afterEach(() => console.log('after each screen size suite'));
    after(() => console.log('after everything'));
    it('should be run for all screen sizes', () => {});
  },
  {
    // Tests for extra small screens
    xs: () => {
      before(() => console.log('before xs'));
      beforeEach(() => console.log('before each xs'));
      afterEach(() => console.log('after each xs'));
      after(() => console.log('after xs'));
      it('xs only test', () => {});
    },
    // Tests for small screens
    sm: () => {
      before(() => console.log('before sm'));
      beforeEach(() => console.log('before each sm'));
      afterEach(() => console.log('after each sm'));
      after(() => console.log('after sm'));
      it('sm only test', () => {});
    },
    // Tests for medium screens
    md: () => {
      before(() => console.log('before md'));
      beforeEach(() => console.log('before each md'));
      afterEach(() => console.log('after each md'));
      after(() => console.log('after md'));
      it('md only test', () => {});
    },
    // Tests for large screens
    lg: () => {
      before(() => console.log('before lg'));
      beforeEach(() => console.log('before each lg'));
      afterEach(() => console.log('after each lg'));
      after(() => console.log('after lg'));
      it('lg only test', () => {});
    },
    // Tests for extra large screens
    xl: () => {
      before(() => console.log('before xl'));
      beforeEach(() => console.log('before each xl'));
      afterEach(() => console.log('after each xl'));
      after(() => console.log('after xl'));
      it('xl only test', () => {});
    }
  }
);
