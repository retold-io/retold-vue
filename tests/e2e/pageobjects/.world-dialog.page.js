// MIT License
// Author: Nick Van Osta

const Page = require('./.page');

class WorldNew extends Page {
  get worlds() {
    return $$('a[href^="/worlds/"]');
  }

  get dialog() {
    return $('#world-dialog');
  }

  get tabInfo() {
    return $('#world-info-tab');
  }

  get tabTheme() {
    return $('#world-theme-tab');
  }

  get tabPreview() {
    return $('#world-preview-tab');
  }

  get inputName() {
    return $('#world-dialog-name');
  }

  get inputImg() {
    return $('#world-dialog-img');
  }

  get inputDescription() {
    return $('#world-dialog-description');
  }

  get inputThemeSwitch() {
    return $('#world-dialog-theme');
  }

  get inputColorPicker() {
    return $$('#world-dialog-color-picker v-color-picker__input');
  }

  get worldPreview() {
    return $('#world-dialog-preview');
  }

  get close() {
    return $('#world-dialog-close');
  }

  get cancel() {
    return $('#world-dialog-cancel');
  }

  get confirm() {
    return $('#world-dialog-confirm');
  }

  getThemeButton(color) {
    return $(`#world-dialog-theme-${color}`);
  }
}

module.exports = WorldNew;
