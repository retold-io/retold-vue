// MIT License
// Author: Xander Veldeman

const Page = require('./.page');
class CharacterList extends Page {
  get list() {
    // Select first found
    return $('#character-list');
  }

  get characters() {
    // Select all from list
    return $$('a[id^="character"]');
  }

  open() {
    return super.open('/characters');
  }
}

module.exports = new CharacterList();
