// MIT License
// Author: Nick Van Osta

const https = require('https');
const fs = require('fs');

let sizes = ['xs', 'sm', 'md', 'lg', 'xl'];
const mobileSizes = ['xs', 'sm'];

const sizeTitles = {
  [sizes[0]]: 'Extra Small',
  [sizes[1]]: 'Small',
  [sizes[2]]: 'Medium',
  [sizes[3]]: 'Large',
  [sizes[4]]: 'Extra Large'
};

const sizeFunctions = {
  [sizes[0]]: async () => await browser.setWindowSize(540, 960),
  [sizes[1]]: async () => await browser.setWindowSize(960, 540),
  [sizes[2]]: async () => await browser.setWindowSize(1280, 720),
  [sizes[3]]: async () => await browser.setWindowSize(1920, 1080),
  [sizes[4]]: async () => await browser.setWindowSize(3840, 2160)
};

const sizesOpt = process.argv.indexOf('--sizes');
if (sizesOpt >= 0) {
  const allowedSizes = process.argv[sizesOpt + 1].split(',');
  sizes = sizes.filter((s) => allowedSizes.includes(s));
}

/**
 * Generates E2E tests for multiple device sizes.
 *
 * @param {(this: Mocha.Suite) => void} fn The mocha tests to be run.
 * @param {{
 *  xs: (this: Mocha.Suite) => void,
 *  sm: (this: Mocha.Suite) => void,
 *  md: (this: Mocha.Suite) => void,
 *  lg: (this: Mocha.Suite) => void,
 *  xl: (this: Mocha.Suite) => void,
 *  mobile: (this: Mocha.Suite) => void,
 *  desktop: (this: Mocha.Suite) => void,
 * }} hooks The mocha tests to be run for specific screen sizes.
 * @returns {Mocha.Suite} The generated mocha test suite.
 * @author Nick Van Osta
 */
module.exports.describeE2E = function describeE2E(title, fn, hooks) {
  return describe(title, () => {
    sizes.forEach((size) =>
      describe(sizeTitles[size], () => {
        const suite = {
          size,
          mobile: mobileSizes.includes(size)
        };

        before(async () => {
          sizeFunctions[size]();
        });
        // after(async () => {});
        // beforeEach(async () => {});
        // afterEach(async () => {});

        fn(suite);

        if (hooks && hooks[size]) hooks[size](suite);
        if (suite.mobile && hooks && hooks.mobile) hooks.mobile(suite);
        if (!suite.mobile && hooks && hooks.desktop) hooks.desktop(suite);
      })
    );
  });
};

/**
 * Downloads a file from the given URL to the given destination.
 *
 * @param {string|URL|https.RequestOptions} url The URL of the file you want to download.
 * @param {fs.PathLike} dest The destination for the downloaded file.
 * @returns {Promise<void>}
 * @author Nick Van Osta
 */
module.exports.download = async function download(url, dest) {
  return new Promise((resolve, reject) => {
    const file = fs.createWriteStream(dest);
    https
      .get(url, (res) => {
        res.pipe(file);
        file.on('finish', () => {
          file.close(resolve);
        });
      })
      .on('error', (err) => {
        fs.unlink(dest);
        reject(err);
      });
  });
};

/**
 * Deletes the given file.
 *
 * @param {fs.PathLike} file The path to file you want to delete.
 * @returns {Promise<void>}
 * @author Nick Van Osta
 */
module.exports.deleteFile = async function deleteFile(file) {
  return new Promise((resolve) => {
    fs.unlink(file, resolve);
  });
};
