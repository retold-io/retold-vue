#!/usr/bin/env node

const shell = require('shelljs');

const PRETTIER_REGEX =
  /^.+\.(html|css|scss|sass|less|js|jsx|ts|vue|json|yaml|yml|md|gql|graphql)$/i;
const VUE_LINT_REGEX = /^.+\.(js|json|vue)$/i;

const NO_CHANGES =
  'No newly created or modified files. Skipping code checks...';

// find changed files
let diff = shell
  .exec('git diff --cached --name-only --diff-filter=d', { silent: true })
  .stdout.trim();

// no staged changes
if (diff.length <= 0) {
  console.error(NO_CHANGES);
  shell.exit(0);
}

// split changed files into list
diff = diff.split('\n');

// run vue lint on applicable files
let lint_diff;
if ((lint_diff = diff.filter((f) => f.match(VUE_LINT_REGEX))).length > 0) {
  const lint = shell.exec(`yarn lint:check ${lint_diff.join(' ')}`);
  lint.code === 0 || shell.exit(lint.code);
}

// run prettier on applicable files
let prettier_diff;
if ((prettier_diff = diff.filter((f) => f.match(PRETTIER_REGEX))).length > 0) {
  const prettier = shell.exec(`yarn prettier:check ${prettier_diff.join(' ')}`);
  prettier.code === 0 || shell.exit(prettier.code);
}
