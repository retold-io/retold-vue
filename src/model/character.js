// MIT License
// Author: Xander Veldeman

export default class Character {
  constructor(id, name, race, gender, thumbnail, description, theme) {
    this.id = id;
    this.name = name;
    this.race = race;
    this.gender = gender;
    this.thumbnail = thumbnail;
    this.description = description;
    this.theme = theme;
  }
}
