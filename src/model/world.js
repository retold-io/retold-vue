// MIT License
// Author: Nick Van Osta

import defaultTheme from '@/plugins/theme';
import { deepClone } from '@u/deep-object';

export default class World {
  constructor(
    id,
    name,
    thumbnail,
    description,
    theme = deepClone(defaultTheme)
  ) {
    this.id = id;
    this.name = name;
    this.thumbnail = thumbnail;
    this.description = description;
    this.theme = theme;
  }
}
