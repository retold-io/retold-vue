// MIT License
// Author: Nick Van Osta

import Vue from 'vue';
import Vuex from 'vuex';

import appbar from '@xm/appbar';
import nav from '@xm/nav';
import footer from '@xm/footer';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    appbar,
    nav,
    footer
  }
});
