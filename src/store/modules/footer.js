// MIT License
// Autor: Nick Van Osta

import NavItem from '@u/nav-item';

// Actions

// Mutations

// Defaults
const DEFAULT_LINKS = [
  new NavItem('https://www.gitlab.com/retold-io', 'GitLab', '$gitlab'),
  new NavItem('https://www.twitter.com', 'Twitter', '$twitter'),
  new NavItem('https://www.facebook.com', 'Facebook', '$facebook'),
  new NavItem('https://www.linkedin.com', 'LinkedIn', '$linkedin')
];

export default {
  namespaced: true,

  state: {
    links: DEFAULT_LINKS
  },

  getters: {},

  mutations: {},

  actions: {}
};
