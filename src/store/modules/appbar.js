// MIT License
// Autor: Nick Van Osta

export const APPBAR_MODULE = 'appbar';

// Actions
export const SET_TITLE = 'set-title';
export const SET_IMG = 'set-img';

export const RESET_TITLE = 'reset-title';
export const RESET_IMG = 'reset-img';

// Mutations
const TITLE_MUT_SET = 'set-title';
const IMG_MUT_SET = 'set-img';

// Defaults
const DEFAULT_TITLE = 'Retold';
const DEFAULT_IMG = '';

export default {
  namespaced: true,

  state: {
    title: DEFAULT_TITLE,
    img: DEFAULT_IMG
  },

  getters: {},

  mutations: {
    [TITLE_MUT_SET]: (state, newTitle) => {
      state.title = newTitle;
    },

    [IMG_MUT_SET]: (state, newImg) => {
      state.img = newImg;
    }
  },

  actions: {
    [SET_TITLE]: ({ commit }, title) => {
      commit(TITLE_MUT_SET, title);
    },

    [SET_IMG]: ({ commit }, img) => {
      commit(IMG_MUT_SET, img);
    },

    [RESET_TITLE]: ({ commit }) => {
      commit(TITLE_MUT_SET, DEFAULT_TITLE);
    },

    [RESET_IMG]: ({ commit }) => {
      commit(IMG_MUT_SET, DEFAULT_IMG);
    }
  }
};
