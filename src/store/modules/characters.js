// MIT License
// Autor: Xander Veldeman

import Character from '@m/character';

export const CHARACTERS_MODULE = 'characters';

// Getters
export const GET_CHARACTER = 'get-character';

// DEFAULTS
const DEFAULT_CHARACTERS = {
  0: new Character(
    0,
    'Lucian',
    'Genasi',
    'Male',
    'https://i.imgur.com/lRbC6Pt.png',
    'The flame of the sea with a pistol to see',
    {
      light: {
        primary: '#d10e00',
        secondary: '#d17d00',
        accent: '#ffb74d'
      }
    }
  ),
  1: new Character(
    1,
    'SL-4Y 3R',
    'Warforged',
    'ROBOT',
    'https://cdn.discordapp.com/attachments/618860171032264704/776812854254764062/SL4Y_3r_screenshot.png',
    'Thank you for purchasing the LoE inc Savage Lookout - 4 Yottabyte 3rd Revision (henceforth abbreviated as SL-4Y 3R). LoE inc presents: our newest state of the art sentry bot capable of protecting any and all of your demonic possession . This version is equipped with 4 Yottabytes (2^80 bytes) of storage powered by our patented Soul Sacrifice™ technology. ',
    {
      light: {
        primary: '#828282',
        secondary: '#7d848c',
        accent: '#c1c9d4'
      }
    }
  )
};

export default {
  namespaced: true,

  state: {
    list: DEFAULT_CHARACTERS
  },

  getters: {
    [GET_CHARACTER]: (state) => (id) => {
      return state.list[id];
    }
  }
};
