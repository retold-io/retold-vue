// MIT License
// Author: Nick Van Osta

/**
 * Deep merges 2 objects/arrays.
 * The first will always be overwritten by the second when overlapping values are found that are not mergeable.
 *
 * @param {*} a The first object/array.
 * @param {*} b The second object/array.
 * @param {[*]} seen A dictionary used to prevent the function getting stuck in circular dependencies.
 * @returns {*} The deep merged result.
 * @author Nick Van Osta
 */
export function deepMerge(a, b, seen = []) {
  if (typeof b === 'function') return b;
  if (Array.isArray(a) && Array.isArray(b)) return [...a, ...b];
  if (a instanceof Object && b instanceof Object)
    for (let k of Object.keys(b)) {
      if (
        !seen.includes(b[k]) &&
        a[k] &&
        ((a[k] instanceof Object && b[k] instanceof Object) ||
          (Array.isArray(a[k]) && Array.isArray(b[k])))
      ) {
        // mark the object as seen
        seen.push(b[k]);

        a[k] = deepMerge(a[k], b[k], seen);
      } else a[k] = b[k];
    }
  else return b;
  return a;
}

/**
 * Deep clones an object/array.
 *
 * @param {*} x The object/array to clone.
 * @returns {*} The deep cloned result.
 * @author Nick Van Osta
 */
export function deepClone(x) {
  const object = x instanceof Object;
  const array = Array.isArray(x);
  let y = x;

  if (typeof x === 'function') return y;

  if ((array && (y = [])) || (object && (y = {})))
    for (let k of Object.keys(x))
      if (x[k] instanceof Object || Array.isArray(x[k])) y[k] = deepClone(x[k]);
      else y[k] = x[k];

  return y;
}
