// MIT License
// Author: Nick Van Osta

class NavItem {
  constructor(to, title, icon = '') {
    this.to = to;
    this.title = title;
    this.icon = icon;
  }
}

export default NavItem;
