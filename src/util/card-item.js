// MIT License
// Author: Nick Van Osta

import World from '@m/world';
import Character from '@m/character';

class CardItem {
  constructor(id, title, img = '') {
    this.id = id;
    this.title = title;
    this.img = img;
  }

  static toCardItem(obj) {
    if (obj instanceof World)
      return new CardItem(obj.id, obj.name, obj.thumbnail);
    if (obj instanceof Character)
      return new CardItem(obj.id, obj.name, obj.thumbnail);
  }
}

export default CardItem;
