// MIT License
// Author: Nick Van Osta

class Gradient {
  constructor(
    { from: colorFrom, to: colorTo },
    { from: opacityFrom = 1, to: opacityTo = 1 } = { from: 1, to: 1 }
  ) {
    this.color = {};
    this.color.from = colorFrom;
    this.color.to = colorTo;

    this.opacity = {};
    this.opacity.from = opacityFrom;
    this.opacity.to = opacityTo;
  }
}

export default Gradient;
