// MIT License
// Author: Nick Van Osta

class FabItem {
  constructor(title, icon, action, color = '') {
    this.title = title;
    this.icon = icon;
    this.action = action;
    this.color = color;
  }
}

export default FabItem;
