// MIT License
// Author: Nick Van Osta, Xander Veldeman

import Vue from 'vue';
import VueRouter from 'vue-router';
import { loadLanguageAsync } from '@/plugins/vue-i18n';

export const HOME = 'home';
export const WORLDS = 'worlds';
export const WORLDS_NEW = 'worlds-new';
export const WORLDS_EDIT = 'worlds-edit';
export const WORLD_DETAILS = 'world-details';
export const CHARACTERS = 'characters';
export const CHARACTER_DETAILS = 'character-details';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: HOME,
    component: () => import('@v/Debug')
  },

  {
    path: '/worlds',
    name: WORLDS,
    component: () => import(/* webpackChunkName: "worlds" */ '@v/WorldList'),
    children: [
      {
        path: 'new',
        name: WORLDS_NEW,
        component: () =>
          import(/* webpackChunkName: "worlds-new" */ '@v/WorldNew')
      },

      {
        path: ':id/edit',
        name: WORLDS_EDIT,
        component: () =>
          import(/* webpackChunkName: "worlds-edit" */ '@v/WorldEdit'),
        props: (route) => ({ id: +route.params.id })
      }
    ]
  },

  {
    path: '/worlds/:id',
    name: WORLD_DETAILS,
    component: () =>
      import(/* webpackChunkName: "world-details" */ '@v/WorldDetails'),
    props: (route) => ({ id: +route.params.id })
  },

  {
    path: '/characters',
    name: CHARACTERS,
    component: () =>
      import(/* webpackChunkName: "characters" */ '@v/CharacterList')
  },

  {
    path: '/characters/:id',
    name: CHARACTER_DETAILS,
    component: () =>
      import(/* webpackChunkName: "character-details" */ '@v/CharacterDetails'),
    props: (route) => ({ id: +route.params.id })
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, _from, next) => {
  const lang = to.params.lang;
  if (lang) await loadLanguageAsync(lang);
  next();
});

export default router;
