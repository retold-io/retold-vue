// MIT License
// Author: Nick Van Osta

export default {
  data() {
    return {
      hover: false
    };
  },

  methods: {
    mouseEnter() {
      this.hover = true;
    },

    mouseLeave() {
      this.hover = false;
    }
  }
};
