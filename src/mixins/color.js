// MIT License
// Author: Nick Van Osta

export default {
  methods: {
    isDark(color) {
      const R = parseInt(color.slice(1, 3), 16);
      const G = parseInt(color.slice(3, 5), 16);
      const B = parseInt(color.slice(5, 7), 16);

      return ((R << 1) + R + B + (G << 2)) >> 3 <= 128;
    }
  }
};
