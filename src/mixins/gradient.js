// MIT License
// Author: Nick Van Osta

import Gradient from '@u/gradient';

const INCORRECT_TYPE_MSG = 'Invalid parameter, should be of type Gradient';

export default {
  methods: {
    createGradient(gradient) {
      if (!(gradient instanceof Gradient)) throw new Error(INCORRECT_TYPE_MSG);

      return {
        from:
          gradient.opacity.from < 1
            ? `rgba(${parseInt(
                gradient.color.from.slice(1, 3),
                16
              )}, ${parseInt(gradient.color.from.slice(3, 5), 16)}, ${parseInt(
                gradient.color.from.slice(5, 7),
                16
              )}, ${gradient.opacity.from})`
            : gradient.color.from,

        to:
          gradient.opacity.to < 1
            ? `rgba(${parseInt(gradient.color.to.slice(1, 3), 16)}, ${parseInt(
                gradient.color.to.slice(3, 5),
                16
              )}, ${parseInt(gradient.color.to.slice(5, 7), 16)}, ${
                gradient.opacity.to
              })`
            : gradient.color.to
      };
    }
  }
};
