// MIT License
// Autor: Nick Van Osta

export default {
  light: {
    primary: '#8F0000',
    secondary: '#3D3D3D',
    accent: '#DEA402',
    background: '#FEFAE0',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  },
  dark: {
    primary: '#8F0000',
    secondary: '#3D3D3D',
    accent: '#DEA402',
    background: '#5E6572',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
};
